import { CACHE_MANAGER, Controller, Get, Inject } from '@nestjs/common';
import { XKCDService } from './xkcd.service';

@Controller('api/v1/xkcd')
export class XKCDController {
  constructor(private readonly service: XKCDService) {}

  @Get('hello')
  getHello(): string {
    return this.service.getHello();
  }

  @Get('latest')
  getLatest(): Promise<number> {
    return this.service.getLatestNumber();
  }

  @Get('random')
  getRandom(): Promise<object> {
    return this.service.getRandom();
  }
}
