import { CacheModule, HttpModule, Module } from '@nestjs/common';
import { XKCDController } from './xkcd.controller';
import { XKCDService } from './xkcd.service';

@Module({
  imports: [HttpModule, CacheModule.register()],
  controllers: [XKCDController],
  providers: [XKCDService],
})
export class XKCDModule {}
