import { Test, TestingModule } from '@nestjs/testing';
import { XKCDController } from './xkcd.controller';
import { XKCDService } from './xkcd.service';

describe('XKCDController', () => {
  let Controller: XKCDController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [XKCDController],
      providers: [XKCDService],
    }).compile();

    Controller = app.get<XKCDController>(XKCDController);
  });

  describe('root', () => {
    it('should return "Hello XKCD!"', () => {
      expect(Controller.getHello()).toBe('Hello XKCD!');
    });
  });
});
