import { CACHE_MANAGER, HttpService, Inject, Injectable } from '@nestjs/common';
import { Cache } from 'cache-manager';

const host = 'http://xkcd.com';
const path = 'info.0.json';

function randomInt(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

@Injectable()
export class XKCDService {
  constructor(
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    private httpService: HttpService,
  ) {}

  getHello(): string {
    return 'Hello XKCD!';
  }

  getLatestNumber(): Promise<number> {
    return new Promise(async (resolve) => {
      const cachedLatest = await this.cacheManager.get('latest');

      if (cachedLatest) {
        return resolve(cachedLatest);
      }

      const observable = this.httpService.get(`${host}/${path}`);

      observable.subscribe({
        next: ({ data: { num } }) => {
          this.cacheManager.set('latest', num, { ttl: 1800 });
          resolve(num);
        },
      });
    });
  }

  async getRandom(): Promise<object> {
    return new Promise(async (resolve) => {
      const latest = await this.getLatestNumber();
      const random = randomInt(1, latest);
      const cachedRandom = await this.cacheManager.get(`random-${random}`);

      if (cachedRandom) {
        return resolve(cachedRandom);
      }

      const observable = this.httpService.get(`${host}/${random}/${path}`);

      observable.subscribe({
        next: ({ data }) => {
          this.cacheManager.set(`random-${random}`, data, {
            // TTL can be much longer, since these don't change, but had to set something :)
            ttl: 60 * 60 * 24,
          });

          resolve(data);
        },
      });
    });
  }
}
