import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { MediaModule } from './media/media.module';
import { UserModule } from './user/user.module';
import { XKCDModule } from './xkcd/xkcd.module';

@Module({
  imports: [AuthModule, UserModule, XKCDModule, MediaModule],
})
export class AppModule {}
