import { Injectable } from '@nestjs/common';
import { Response } from 'express';

const request = require('request');

const host = 'https://imgs.xkcd.com/comics/';

@Injectable()
export class MediaService {
  getXKCDImage(file: string, res: Response) {
    request
      .get(`${host}${file}`)
      .on('response', (response) => {
        // modification for headers can be done here, for example to make caching a bit more aggressive
        response.headers['Cache-Control'] = `max-age=${60 * 60 * 24 * 7}`;
      })
      .pipe(res);
  }
}
