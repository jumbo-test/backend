import { Controller, Get, Header, Param, Res } from '@nestjs/common';
import { Response } from 'express';
import { MediaService } from './media.service';

@Controller('api/v1/media')
export class MediaController {
  constructor(private readonly service: MediaService) {}

  @Get('xkcd-image/:file*')
  getXKCDImage(@Param('file') file: string, @Res() res: Response) {
    return this.service.getXKCDImage(file, res);
  }
}
