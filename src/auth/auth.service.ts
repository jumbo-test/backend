import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserBare } from 'src/user/user.interfaces';
import { UserService } from 'src/user/user.service';
import { AccessTokenResponse } from './auth.interfaces';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async validate(username: string, password: string): Promise<UserBare> {
    const user = await this.userService.findOne(username);

    if (user && user.password === password) {
      const { password, ...data } = user;

      return data;
    }

    return null;
  }

  async login(): Promise<AccessTokenResponse> {
    const payload = { username: 'daanvanham', sub: 'abc' };

    return { access_token: this.jwtService.sign(payload) };
  }

  async me(): Promise<UserBare> {
    const user = await this.userService.findOne('daanvanham');

    const { password, ...data } = user;

    return data;
  }
}
