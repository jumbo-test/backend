import { Controller, Get, Post, UseGuards } from '@nestjs/common';
import { UserBare } from 'src/user/user.interfaces';
import { AccessTokenResponse } from './auth.interfaces';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './jwt-auth.guard';
import { LocalAuthGuard } from './local-auth.guard';

@Controller('api/v1/auth')
export class AuthController {
  constructor(private readonly service: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  login(): Promise<AccessTokenResponse> {
    return this.service.login();
  }

  @UseGuards(JwtAuthGuard)
  @Get('me')
  me(): Promise<UserBare> {
    return this.service.me();
  }
}
