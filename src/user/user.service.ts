import { Injectable } from '@nestjs/common';
import { User } from './user.interfaces';

@Injectable()
export class UserService {
  private readonly users = [
    {
      username: 'daanvanham',
      password: 'superstrongpassword',
      fullname: 'Daan van Ham',
      age: 29,
    },
  ];

  async findOne(username: string): Promise<User | undefined> {
    return this.users.find((user) => user.username === username);
  }
}
