export interface UserBare {
  username: string;
  fullname: string;
  age: number;
}

export interface User extends UserBare {
  password: string;
}
